To Download click the small download cloud-looking button on the right of the screen.

To launch game:
- Download or Pull folder BlackJack_ReleaseBuild.
- Launch GreyJack.exe

To view solution:
- Download or Pull folder BlackJack_Solution.
- Open GreyJack.sln in your IDE or double click it.