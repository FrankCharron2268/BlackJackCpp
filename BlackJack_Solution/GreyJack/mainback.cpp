///===================================================///
///POUR UN AFFICHAGE OPTIMAL OUVRER LE PROGRAMME,     ///
///CLICK DROIT SUR LA BARRE BLANCHE DANS LE HAUT DE LA///
///CONSOLE. PLACEZ LA TAILLE DU TEXTE A 20 ET LA      ///
/// COULEUR DU FOREGROUND A LA COULEUR DE VOTRE CHOIX ///
///===================================================///
#include <iostream>
#include <fcntl.h> //pour mes unicodes
#include <io.h>	//setmode API
#include <windows.h>
#include <time.h>
#include <iomanip>
#include <conio.h>
#include <dwrite.h>
#include <cwchar>
#include <string>

using std::string;						 ///===================================================///
using std::wcout;						///FAITES CTRL+A POUR TOUT SELECTIONNER ET ENSUITE    ///
using std::cin;                        ///FAIRE CTRL+M 2X POUR TOUT OUVRIR OUTLINES AU LIEU  ///
using std::printf;					  ///DES LES OUVRIRS 1 PAR 1, IL Y EN A ENVIRON 145     ///
using std::endl;					 ///LES OUVRIR MANUELLEMENT RISQUE DETRE LONG XD ENJOY!///
using std::wostream;				///===================================================///
using std::setw;

#pragma region Functions Definitions
void setFont()
{
	//::SendMessage(::GetConsoleWindow(), WM_SYSKEYDOWN, VK_RETURN, 0x20000000);/// Met la console en FullScreen
	setlocale(LC_ALL, "");
	HANDLE outcon = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_FONT_INFOEX font;
	GetCurrentConsoleFontEx(outcon, false, &font);
	SetConsoleTextAttribute(outcon, 0x02); ///couleure du texte

	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.nFont = 0;
	cfi.dwFontSize.X = 0;
	cfi.dwFontSize.Y = 19;                 ///set la taille du texte
	cfi.FontFamily = FF_DONTCARE;
	cfi.FontWeight = FW_NORMAL;

	wcscpy(cfi.FaceName, L"Lucida Console"); // Choose your font
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &cfi);
}
void setCursorXY(int x, int y)
{								    //=====================================================//
									//Fonction place le curseur selon les parametres recus //
									//=====================================================//
	COORD p = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}
void playerName(char playername[])
{ //=============================================//
 //Fonction qui recoit & print le nom du joueur //
//=============================================//
	for (int i = 0; i < strlen(playername); i++)
	{
		wcout << playername[i];
	}
}
void intro(int width, int height)
{
	for (int Y = 0; Y < 17; Y++)
	{
		Sleep(120);
		system("cls");
		setCursorXY(66, 0 + Y);
		wcout << "----------  BlackJack  ---------" << endl;
		for (int abc = 0; abc < 32; abc++)
		{
			setCursorXY(66 + abc, 1 + Y);
			wcout << L"\u00AF";
		}
		for (int X = 0; X < 4; X++)
		{
			wcout << " ";
			for (int i = 0; i < width - 2; i++)				//=================//
			{											   // Dessine le haut //
				setCursorXY(67 + i + X * 8, 2 + Y);		  //=================//
				wcout << "_";
			}
			wcout << " \n";

			for (int i = 0; i < height - 2; i++)           //============================================================//
			{											  // Cette fonction affiche la page d'introduction ou l'on peux //
				setCursorXY(66 + X * 8, 3 + i + Y);	     // voir Grey Jack, Suivi en dessous des cartes As,V,Q,K	   //
				wcout << "|";							//============================================================//
				for (int j = 0; j < width - 5; j++)    //===================//
				{									  // Dessine les cot�s //
					if (i == 0 && j == 0)			 // & linterieur carte//
					{								//===================//
						switch (X)
						{
						case 0: wcout << L"J\u2665";
							break;
						case 1: wcout << L"Q\u2660";
							break;
						case 2:wcout << L"K\u2666";
							break;
						case 3:wcout << L"A\u2666";
							break;
						}
					}
					else if (i == 3 && j == 2)
					{
						switch (X)
						{
						case 0: wcout << L"J\u2660";
							break;
						case 1: wcout << L"Q\u2666";
							break;
						case 2:wcout << L"K\u2665";
							break;
						case 3:wcout << L"A\u2663";
							break;
						}
					}
					else

						wcout << "  ";
				}
				wcout << "|\n";
			}

			wcout << " ";
			for (int i = 0; i < width - 2; i++)       //==============//
			{									     //Dessine le bas//
				setCursorXY(67 + i + X * 8, 7 + Y); //==============//
				wcout << L"\u00AF"; // upperscore
			}
			wcout << " \n";
		}
	}
}
void introrules(int width, int height)
{
	char rules[] =
	{ "  ____________________________________________________________________________________________________ "
		" |Le Greyjack est une version simplifi� du BlackJack!                                                 |"
		" |Les r�gles sont sensiblement les m�mes:                                                             |"
		" |                                                                                                    |"
		" |-Les joueurs commencent la partie avec 100$.                                                        |"
		" |-Au d�but de chaque nouvelle main, 10$ sera automatiquement pr�lev� de chaque joueur.               |"
		" |-Une main se joue sur 5 tours. Apr�s le 5iem tour, la partie sera automatiquement termin�e.         |"
		" |-Le joueur qui aura alors la main la plus forte, remporte la mise.                                  |"
		" |-Si 2 ou plusieurs joueurs ont une main �quivalente, la mise sera divis�e entre chacun des joueurs. |"
		" |-Au premier tour, chaque joueur re�oit 2 cartes, la premi�re restera face cach�e jusqu'� la fin.    |"
		" |-Pour chaque tour suivant, vous pouvez demander une carte de plus ou passer.                        |"
		" |-L'As vaut 1 ou 11.                                                                                 |"
		" |-Les cartes 2 � 10 ont leurs valeurs normales.                                                      |"
		" |-les figures valent toutes 10.                                                                      |"
		" |-Le but du jeu est de r�ussir � obtenir 21 ou de terminer avec la main la plus forte.               |"
		" |-D�passez 21 et vous perdez.                                                                        |"
		" |Valeurs des Mains:                                                                                  |"
		" |                                                                                                    |"
		" |-1.Natural  :   21 au premier tour(ex: AS+ROI), il remporte automatiquement la main.                |"
		" |-2.BlackJack:   21 avec plus de 2 cartes.                                                           |"
		" |-3.High-Hand:   La valeur sous 21, la plus haute.                                                   |"
		" |-4.Busted   :   Si vous d�passez 21 vous perdez automatiquement la main.                            |"
	};

	for (int X = 0; X < 4; X++)
	{
		wcout << " ";
		for (int i = 0; i < width - 2; i++)				//=================//
		{											   // Dessine le haut //
			setCursorXY(67 + i + X * 8, 9);		      //=================//
			wcout << "_";
		}
		wcout << " \n";

		for (int i = 0; i < height - 2; i++)           //============================================================//
		{											  // Cette fonction affiche la page d'introduction ou l'on peux //
			setCursorXY(66 + X * 8, 10 + i);	     // voir Grey Jack, Suivi en dessous des cartes As,V,Q,K	   //
			wcout << "|";							//============================================================//
			for (int j = 0; j < width - 5; j++)    //===================//
			{									  // Dessine les cot�s //
				if (i == 0 && j == 0)			 // & linterieur carte//
				{								//===================//
					switch (X)
					{
					case 0: wcout << L"J\u2666";
						break;
					case 1: wcout << L"Q\u2666";
						break;
					case 2:wcout << L"K\u2666";
						break;
					case 3:wcout << L"A\u2666";
						break;
					}
				}
				else if (i == 3 && j == 2)
				{
					switch (X)
					{
					case 0: wcout << L"J\u2663";
						break;
					case 1: wcout << L"Q\u2663";
						break;
					case 2:wcout << L"K\u2663";
						break;
					case 3:wcout << L"A\u2663";
						break;
					}
				}
				else

					wcout << "  ";
			}
			wcout << "|\n";
		}
		wcout << " ";
		for (int i = 0; i < width - 2; i++)    //==============//
		{									  //Dessine le bas//
			setCursorXY(67 + i + X * 8, 14); //==============//
			wcout << L"\u00AF"; // upperscore
		}
		wcout << " \n";
	}

	/////////////////////////////////////////////////////// CARTES DU BAS EN BAS DE CETTE LIGNE //////////////////////////////////////////////////
	for (int X = 0; X < 4; X++)
	{
		wcout << " ";
		for (int i = 0; i < width - 2; i++)				//=================//
		{											   // Dessine le haut //
			setCursorXY(67 + i + X * 8, 38);		  //=================//
			wcout << "_";
		}
		wcout << " \n";

		for (int i = 0; i < height - 2; i++)           //============================================================//
		{											  // Cette fonction affiche la page d'introduction ou l'on peux //
			setCursorXY(66 + X * 8, 39 + i);	     // voir Grey Jack, Suivi en dessous des cartes As,V,Q,K	   //
			wcout << "|";							//============================================================//
			for (int j = 0; j < width - 5; j++)    //===================//
			{									  // Dessine les cot�s //
				if (i == 0 && j == 0)			 // & linterieur carte//
				{								//===================//
					switch (X)
					{
					case 0: wcout << L"J\u2665";
						break;
					case 1: wcout << L"Q\u2665";
						break;
					case 2:wcout << L"K\u2665";
						break;
					case 3:wcout << L"A\u2665";
						break;
					}
				}
				else if (i == 3 && j == 2)
				{
					switch (X)
					{
					case 0: wcout << L"J\u2660";
						break;
					case 1: wcout << L"Q\u2660";
						break;
					case 2:wcout << L"K\u2660";
						break;
					case 3:wcout << L"A\u2660";
						break;
					}
				}
				else

					wcout << "  ";
			}
			wcout << "|\n";
		}

		wcout << " ";
		for (int i = 0; i < width - 2; i++)    //==============//
		{									  //Dessine le bas//
			setCursorXY(67 + i + X * 8, 43); //==============//
			wcout << L"\u00AF"; // upperscore
		}
		wcout << " \n";
	}
	///PARTIE QUI PRINT LE TEXTE UNE LETTRE A LA FOIS
	int strcnt = 0;
	for (int i = 0; i < 23; i++)
	{
		for (int j = 0; j < 103; j++)
		{
			++strcnt;
			if (i == 3)
			{
				for (int k = 1; k < 39; k++)         //====================================================================//
				{									// Fait le souslign� sous : "Les regles sont sensiblements les memes:"//
					setCursorXY(35 + k, 15 + i);   //====================================================================//
					wcout << L"\u00AF";//upperscore
				}
				setCursorXY(35, 15 + i);
				wcout << "|";
				setCursorXY(136, 15 + i);
				wcout << "|";
			}
			else if (i == 17)
			{
				for (int k = 1; k < 18; k++)
				{
					setCursorXY(35 + k, 15 + i);       //===============================================//
					wcout << L"\u00AF";//upperscore	  // fait le souslign� sous:" Valeurs des mains:"  //
				}									 //===============================================//
				setCursorXY(35, 15 + i);
				wcout << "|";
				setCursorXY(136, 15 + i);
				wcout << "|";
			}
			else if (i == 22)
			{
				for (int k = 1; k < 101; k++)
				{									//===============================================//
					setCursorXY(35 + k, 15 + i);   // ligne au bas de la boite contenant les regles //
					wcout << L"\u00AF";			  //===============================================//
				}
			}
			else
			{
				Sleep(20);
				setCursorXY(35 + j, 15 + i);     //=======================================//
				wcout << rules[strcnt];			// print le text a linterieur de la boite//
			}								   //=======================================//
		}
	}
}

int getMaxHand(int ValHandTab[4][5], int player)
{  //==========================================================//
  //   Recoit le tab[] contenant les mains des joueurs.       //
 // les compares et retourne la valeur de la main la + haute.//
//==========================================================//

	int compareHandTab[4];
	int maxVal = 0;

	for (int i = 0; i < 4; i++)
	{
		int hand = 0;
		for (int j = 0; j < 5; j++)
		{
			hand += ValHandTab[i][j];
		}
		compareHandTab[i] = hand;
	}//==================================================================//
	//place la valeur de chaque main dans un tab[] temp, et compare sil //
   //est >= a maxVal mais en etant plus <= a 20. Ceci servira a        //
  //determiner si le joueur a un high-hand                            //
 //==================================================================//
	for (int val = 0; val < 4; val++)
	{
		if (compareHandTab[val] <= 20 && compareHandTab[val] >= maxVal)
		{
			maxVal = compareHandTab[val];
		}
	}

	return maxVal;
}
int getCardsValue(int ValHandTab[4][5], int player)
{
	int CardCount = 0;
	int HandValue = 0;
	int AsCount = 0;

	///count player cards.
	for (int i = 0; i < 5; i++)
	{
		///If Null then no Card yet.
		if (ValHandTab[player][i] != NULL)
		{					//====================================================================//
			++CardCount;   // Si valeur n'est pas nul compte une carte pour la main, ++CardCount //
		}				  //====================================================================//
	}

	for (int j = 0; j < CardCount; j++)
	{
		if (ValHandTab[player][j] == 11)
		{				//==========================================================//
			++AsCount; //Si la valeur est de 11,cest un As et on incremente AsCount//
		}			  //==========================================================//

		HandValue += ValHandTab[player][j]; //Contatenation de chaque mains//
	}

	//Adjust score based on Ace count			//========================================================//
	if (HandValue > 31 && AsCount >= 1)	       //Ex : Si on a 32 ou + et quon a 2 As, on enleve -10 par  //
	{										  //As pour retomber a 12 si on enleve qu'un on est over 22	//
		HandValue -= 10 * AsCount;           //========================================================//
	}
	else if (HandValue > 21 && AsCount >= 1)
	{									//================================================================//
		HandValue -= 10;			   //Dans ce cas ci on reduit de 10 lorsque As > 1 et handvalue > 21 //
	}								  //Ce else if rend probablement le If ci dessus inutile------------//
	return HandValue;				 //================================================================//
}
boolean checkforWinner(int ValHandTab[4][5], int wallet[], int HandStatusTab[4][2], char playername[])//TODO
{
	int maxVal = -1;
	int equalCount = 0;
	int outCount = 0;
	boolean resultat;
	for (int i = 0; i < 4; i++)
	{										  //======================================================================//
		if (HandStatusTab[i][1] > maxVal)    //La colone 1 de HandStatusTab contient un code pour chaque main        //
		{									//-1 = busted 0 = Rien, 2 == HighHand, 3 = BlackJack, 4= Natural        //
			maxVal = HandStatusTab[i][1];  //Cette boucle determine quel est la main la plus forte jusqua present  //
		}								  //La colone 2 contient 0 ou 1, 1 = out. Lorsque = 1, incremente outCount//
		if (HandStatusTab[i][2] == 1)    //la partie ce termine lorsque outcount == 4.===========================//
		{
			++outCount;
		}
	}
	for (int i = 0; i < 4; i++)						   //=============================================================//
	{												  // une fois la maxVal trouv�e on verifie si il y a une egalit� //
		if (HandStatusTab[i][1] == maxVal)           // en recomparant chaque code de main avec maxVal, Si egale,   //
		{											// equalCount sera increment�.=================================//
			++equalCount;
		}
	}
	if (outCount >= 4)
	{
		int loot = wallet[4];						//============================================================//
		wallet[4] = 0;							   // La Partie est termin� quand outCount >= 4, on calcule alors//
		for (int j = 0; j < 4; j++)				  // la distribution de la mise et comparant la main la plusfort//
		{										 // avec la valeur de maxVal et en le total de la mise par     //
			if (HandStatusTab[j][1] == maxVal)  // equalCount.=================================================//
			{
				wallet[j] += loot / equalCount;

				if (j != 3)
				{
					setCursorXY(65, 45 + j);
					wcout << "Player: " << j + 1 << " won the hand" << endl;
				}                                    //===================================================//
				else                                //Lorsque j !=3 cest un AI donc on print Player (..) //
				{								   // Sinon = Player, on appel donc playerName pour voir//
					setCursorXY(65, 45 + j);	  // le nom du joueur==================================//
					playerName(playername);
					//	playerName(playername);wcout << "Frank ";
					wcout << " won the hand" << endl;
				}
			}
		}
		resultat = false;  // False = game over
	}
	else
	{
		resultat = true; // true = partie continue
	}
	return resultat;
}

void draw_rect(int width, int height, int I, int J, const wchar_t* CardHandTab[4][5], int ValHandTab[4][5])
{
	wcout << " ";
	for (int i = 0; i < width - 2; i++)                       //==============================================//
	{														 //Cette Fonction a le meme frame que Intro mais //
		setCursorXY(67 + i + J * 8, 17 + I * 6);			//sera appel� plus souvent et sont afficha varie//
		wcout << "_";									   //selon le besoin ==============================//
	}
	wcout << " \n";

	for (int i = 0; i < height - 2; i++)
	{
		setCursorXY(66 + J * 8, 18 + i + I * 6);
		wcout << "|";

		for (int j = 0; j < width - 5; j++)
		{
			/* ==================================================
				Jai rajouter le premier if ci dessous pour cacher
				les premieres cartes des 3 premier jouers,
				J 0 etant la premiere carte de chaque mains
				et I 3 etant le 4iem joueur
			================================================== */
			if (J == 0 && I != 3)
			{
				wcout << "  ";
			}
			else
			{
				if (i == 0 && j == 0)
					wcout << CardHandTab[I][J];
				else if (CardHandTab[I][J] == L"10\u2665" && j == 1 && i == 1 || CardHandTab[I][J] == L"10\u2666" && j == 1 && i == 1
					//===========================================================================================================//
				   // Si J != 0 (else) on verra, si a la position i==0 et j ==0 la figure de la carte dans le coin haut gauche* //
				  //  le premier else if gere le cas du 10 qui est un caractere plus long et decalait les cotes de la carte*   //
				 //  le 2iem else if est pour la figure en bas a droite*													  //
				//===========================================================================================================//
					|| CardHandTab[I][J] == L"10\u2660" && j == 1 && i == 1 || CardHandTab[I][J] == L"10\u2663" && j == 1 && i == 1)
					wcout << "\b";
				else if (i == 3 && j == 2)
					wcout << CardHandTab[I][J];

				else
					wcout << "  ";
			}
		}
		wcout << "|\n ";
	}

	wcout << " ";
	for (int i = 0; i < width - 2; i++)
	{
		setCursorXY(67 + i + J * 8, 22 + I * 6);
		wcout << L"\u00AF";
	}
	wcout << " \n";
}
void draw_rectnoconditions(int width, int height, int I, int J, const wchar_t* CardHandTab[4][5], int ValHandTab[4][5])
{
	for (int a = 0; a < 4; a++)
	{									    //==============================================================//
		int b = 0;					       // Fonction identique a Draw_rect mais elle na pas la condi-    //
										  // tion qui cache la 1er carte cet elle sera appeler qu'a la fin//
										 // de la partie pour reveler la premiere carte de chaque joueur //
		wcout << " ";				    //==============================================================//
		for (int i = 0; i < width - 2; i++)
		{
			setCursorXY(67 + i + b * 8, 17 + a * 6);
			wcout << "_";
		}
		wcout << " \n";

		for (int i = 0; i < height - 2; i++)
		{
			setCursorXY(66 + b * 8, 18 + i + a * 6);
			wcout << "|";

			for (int j = 0; j < width - 5; j++)
			{
				if (i == 0 && j == 0)
					wcout << CardHandTab[a][b];
				else if (CardHandTab[a][b] == L"10\u2665" && j == 1 && i == 1 || CardHandTab[a][b] == L"10\u2666" && j == 1 && i == 1
					//==========================================================================================================//
				   // Si J != 0 (else) on verra, si a la position i==0 et j ==0 la figure de la carte dans le coin haut gauche*//
				  // le premier else if gere le cas du 10 qui est un caractere plus long et decalait les cotes de la carte*   //
				 // le 2iem else if est pour la figure en bas a droite*						   							     //
				//==========================================================================================================//
					|| CardHandTab[a][b] == L"10\u2660" && j == 1 && i == 1 || CardHandTab[a][b] == L"10\u2663" && j == 1 && i == 1)
					wcout << "\b";
				else if (i == 3 && j == 2)
					wcout << CardHandTab[a][b];

				else
					wcout << "  ";
			}

			wcout << "|\n ";
		}

		wcout << " ";
		for (int i = 0; i < width - 2; i++)
		{
			setCursorXY(67 + i + b * 8, 22 + a * 6);
			wcout << L"\u00AF";
		}
		wcout << " \n";
	}
}

///Count Players Current Hand Score
void showHands(int ValHandTab[4][5], const wchar_t* CardHandTab[4][5], int wallet[], int HandStatusTab[4][2], char playername[])
{
	int A = 8;
	int B = 6;
	//string resultat = "";
	int resultat = 0;
	setCursorXY(65, 15);
	wcout << "----------*!BlackJack!*---------" << endl;
	setCursorXY(40, 18);
	wcout << "Valeur de la mise " << wallet[4] << "$";
	for (int i = 0; i < 4; i++)
	{
		Sleep(500);

		if (i != 3)
		{
			setCursorXY(40, 20 + i * 6);
			wcout << "Player" << i + 1 << ":" << setw(2);
			setCursorXY(40, 21 + i * 6);
			wcout << wallet[i] << "$";
			wcout << endl;
		}		//=========================================//
		else   // si i != on imprime player + # sinon on  //
		{	  // print le nom du joueur==================//
			setCursorXY(40, 20 + i * 6);
			playerName(playername);
			//wcout << "->Frank:";
			wcout << ":" << setw(2);
			setCursorXY(40, 21 + i * 6);
			wcout << wallet[i] << "$";
			wcout << endl;
		}

		///Calcul la valeur total de chaque mains

		for (int j = 0; j < 5; j++)
		{
			if (CardHandTab[i][j] != NULL)
			{///Tien compte seulement si la valeure nest PAS null
				wcout << endl;										//======================================//
				draw_rect(A, B, i, j, CardHandTab, ValHandTab);    //Si CardHandTab[i][j] != NULL, call    //
			}													  //la fonction qui affiche les cartes    //
		}														 //en lui passant comme param le symbole //
																// de la carte ex = L"k\u2665" =========//

		if (getCardsValue(ValHandTab, i) > 21)
		{
			setCursorXY(110, 20 + i * 6);

			wcout << getCardsValue(ValHandTab, i) << " " << " Busted!       ";
			HandStatusTab[i][1] = -1;
			HandStatusTab[i][2] = 1;
		}
		else if (ValHandTab[i][2] == 0 && getCardsValue(ValHandTab, i) == 21)
		{
			setCursorXY(110, 20 + i * 6);
			wcout << getCardsValue(ValHandTab, i) << " " << " Natural!     ";
			HandStatusTab[i][1] = 4;											   //=========================================================================//
			for (int a = 0; a < 4; a++)											  // Cette serie de if determine le type de main et affiche son nom si les   //
			{																	 //	conditions sont respect�es. HandStatusTab[i][1] recevra le code pour le //
				HandStatusTab[a][2] = 1;										//  type de main en [i][1] ex:-1busted 0rien 2highhand 3blackjack 4natural //
			}																   // et selon le cas HandStatusTab[i][2] recevra 1 pour out                  //
		}																	  // ex : joueur 1 a Natural. Tout le monde recoit 1 pour out                //
		else if (getCardsValue(ValHandTab, i) == 21 && ValHandTab[i][2] > 0) // puisqu'il a gagner la partie. Si BlackJack lui seul recoit un 1 puisquil//
		{																	// na pas besoin de continuer mais les autre peuvent le rattraper.         //
			setCursorXY(110, 20 + i * 6);								   // Note  que 1 = out mais out ne signifie pas !!perdant!!==================//
			wcout << getCardsValue(ValHandTab, i) << " " << " BlackJack!       ";
			HandStatusTab[i][1] = 3;
			HandStatusTab[i][2] = 1;
		}
		else if (getCardsValue(ValHandTab, i) < 21 && getMaxHand(ValHandTab, i) == getCardsValue(ValHandTab, i))
		{
			setCursorXY(110, 20 + i * 6);
			wcout << getCardsValue(ValHandTab, i) << " " << " High-Hand";
			HandStatusTab[i][1] = 2;
		}
		else
		{
			setCursorXY(110, 20 + i * 6);
			HandStatusTab[i][1] = 0;
			HandStatusTab[i][2] = 0;
			wcout << getCardsValue(ValHandTab, i) << "                      ";
		}

		wcout << endl;
	}
}

void drawCards(const wchar_t* CardTab[], int ValTab[], const wchar_t* CardHandTab[4][5], int ValHandTab[4][5])
{///distribue 2 cartes a chaque joueurs ( joueur = lignes, cartes = cols)
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 2; j++)
		{///tire un nombre au hazard
			int x = rand() % 51 + 0;			 //=====================================================================================//
			do                                  //distribue 2 cartes a chaque joueurs ( joueur = lignes, cartes = cols)                //
			{								   //verifie que lindex tirer au hasard na pas ete placer null lors	                      //
				x = rand() % 51 + 0;		  //dun tirage precedent sinon refait le rand jusqua ce que lindex correspondant !=null) //
			} while (CardTab[x] == NULL);	 //selon le # generer dans par rand(), tire la carte correspondante a lindex dans       //
			CardHandTab[i][j] = CardTab[x]; // les 2 tab de mains(card/val) place la carte a null pour ne pas avoirs de doubles    //
			ValHandTab[i][j] = ValTab[x];  // et la valeur aussi est plac�e a null ou plutot 0                                    //
			CardTab[x] = NULL;			  //=====================================================================================//
			ValTab[x] = 0;
		}
	}

	system("cls");
}

void anotherCard(const wchar_t* CardTab[], int ValTab[], const wchar_t* CardHandTab[4][5], int ValHandTab[4][5], int wallet[], int HandStatusTab[4][2], char playername[])
{///demande au joueurs sils veullent une autre carte.
	for (int i = 0; i < 4; i++)
	{
		if (HandStatusTab[i][2] != 1 && checkforWinner(ValHandTab, wallet, HandStatusTab, playername) == true)
			//if (getCardsValue(ValHandTab, i) < 21 && checkforWinner(ValHandTab, wallet, HandStatusTab, playername)  == true)
		{
			Sleep(500);
			int CardCount = 0;
			char answer = ' ';
			int HandValue = getCardsValue(ValHandTab, i);
			if (i <= 2)
			{
				setCursorXY(65, 45);
				wcout << "Player " << i + 1 << ", " << "Desirez vous une autre carte?" << endl; // demande au joueur AI sil veut une carte
				Sleep(1000);
				setCursorXY(65, 45);
				wcout << "                                                     " << endl; //efface la ligne

				if (HandValue <= 17)
				{						//======================================================//
					answer = 'y';	   // si le joueur 1-3 a <= 17 il demandera une autre carte//
				}					  //======================================================//
				else
				{
					answer = 'n';
				}
			}
			else if (i == 3)
			{
				setCursorXY(65, 45);
				playerName(playername);
				//wcout << "Frank, " ;
				wcout << ", Desirez vous une autre carte? y/n      " << endl;  // demande au joueur sil veut une carte
				Sleep(1000);
				setCursorXY(65, 47);
				cin >> answer;

				setCursorXY(65, 45);
				wcout << "                                                     " << endl;//efface la ligne
				setCursorXY(65, 47);
				wcout << "                         ";//efface la ligne
			}

			if (answer == 'y' || answer == 'Y') // jaurais du faire un while .. pas grave ca revien au meme
			{
				for (int j = 0; j < 5; j++)
				{
					if (ValHandTab[i][j] != 0)	   //=================================================================//
					{							  //SI le joueur a entr� Y/y on ajoute une carte a sa main           //
						CardCount++;			 // incremente cardcount qui sert de compteur pour savoir a quelle  //
					}							// colonne distribuer la carte !                                   //
				}							   //=================================================================//
				setCursorXY(40, 33);

				if (i != 3)
				{
					setCursorXY(65, 44);
					wcout << "Player " << i + 1 << ":- Oui!                       ";
					setCursorXY(65, 44);
					Sleep(1000);
					wcout << "                                         ";
				}
				else
				{
					setCursorXY(65, 44);
					playerName(playername);
					//wcout << "Frank ";
					wcout << ":- Oui!                    ";
					setCursorXY(65, 44);
					Sleep(1000);
					wcout << "                                         ";
				}

				int x;
				do                                              //=====================================================================================//
				{											   //                                                                                     //
					x = rand() % 51 + 0;					  //verifie que lindex tirer au hasard na pas ete placer null lors	                     //
				} while (CardTab[x] == NULL);			     //dun tirage precedent sinon refait le rand jusqua ce que lindex correspondant !=null) //
				CardHandTab[i][CardCount] = CardTab[x];	    //selon le # generer dans par rand(), tire la carte correspondante a lindex dans       //
				ValHandTab[i][CardCount] = ValTab[x];	   // les 2 tab de mains(card/val) place la carte a null pour ne pas avoirs de doubles    //
				CardTab[x] = NULL;						  // et la valeur aussi est plac�e a null ou plutot 0                                    //
				ValTab[x] = 0;							 //=====================================================================================//

				showHands(ValHandTab, CardHandTab, wallet, HandStatusTab, playername);///fonction qui affiche le total de chaque mains
			}

			else
			{
				/// [i][2] = 1 designe un player qui est out
				if (i != 3)
				{
					HandStatusTab[i][2] = 1;
					setCursorXY(65, 44);
					wcout << "Player " << i + 1 << ":- Non Merci!!    ";
					setCursorXY(65, 44);
					Sleep(1000);
					wcout << "                                         ";
				}
				else
				{
					HandStatusTab[i][2] = 1;
					setCursorXY(65, 44);
					playerName(playername);
					//wcout << "Frank ";
					wcout << " :- Non Merci!!     ";
					setCursorXY(65, 44);
					Sleep(1000);
					wcout << "                                         ";
				}
			}
		}
	}
}

#pragma endregion

int main()
{
#pragma region system directives

	srand(time(NULL));///determine le parametre de base de la fonction Rand qui sera calculer selon l'heure pour ne pas avoir la meme sequence
	::SendMessage(::GetConsoleWindow(), WM_SYSKEYDOWN, VK_RETURN, 0x20000000);/// Met la console en FullScreen
	_setmode(_fileno(stdout), _O_U16TEXT); ///donne access au characteres speciaux
	setlocale(LC_ALL, "");

	setFont();

#pragma endregion
#pragma region Cards & vars
	///declaration des Rouges
	const wchar_t H1[] = L"A\u2665"; const wchar_t D1[] = L"A\u2666";
	const wchar_t H2[] = L"2\u2665"; const wchar_t D2[] = L"2\u2666";
	const wchar_t H3[] = L"3\u2665"; const wchar_t D3[] = L"3\u2666";
	const wchar_t H4[] = L"4\u2665"; const wchar_t D4[] = L"4\u2666";
	const wchar_t H5[] = L"5\u2665"; const wchar_t D5[] = L"5\u2666";
	const wchar_t H6[] = L"6\u2665"; const wchar_t D6[] = L"6\u2666";
	const wchar_t H7[] = L"7\u2665"; const wchar_t D7[] = L"7\u2666";
	const wchar_t H8[] = L"8\u2665"; const wchar_t D8[] = L"8\u2666";
	const wchar_t H9[] = L"9\u2665"; const wchar_t D9[] = L"9\u2666";
	const wchar_t H10[] = L"\b10\u2665"; const wchar_t D10[] = L"\b10\u2666";
	const wchar_t H11[] = L"J\u2665"; const wchar_t D11[] = L"J\u2666";
	const wchar_t H12[] = L"Q\u2665"; const wchar_t D12[] = L"Q\u2666";
	const wchar_t H13[] = L"K\u2665"; const wchar_t D13[] = L"K\u2666";
	/// Declaration des noirs
	const wchar_t S1[] = L"A\u2660"; const wchar_t C1[] = L"A\u2663";
	const wchar_t S2[] = L"2\u2660"; const wchar_t C2[] = L"2\u2663";
	const wchar_t S3[] = L"3\u2660"; const wchar_t C3[] = L"3\u2663";
	const wchar_t S4[] = L"4\u2660"; const wchar_t C4[] = L"4\u2663";
	const wchar_t S5[] = L"5\u2660"; const wchar_t C5[] = L"5\u2663";
	const wchar_t S6[] = L"6\u2660"; const wchar_t C6[] = L"6\u2663";
	const wchar_t S7[] = L"7\u2660"; const wchar_t C7[] = L"7\u2663";
	const wchar_t S8[] = L"8\u2660"; const wchar_t C8[] = L"8\u2663";
	const wchar_t S9[] = L"9\u2660"; const wchar_t C9[] = L"9\u2663";
	const wchar_t S10[] = L"\b10\u2660"; const wchar_t C10[] = L"\b10\u2663";
	const wchar_t S11[] = L"J\u2660"; const wchar_t C11[] = L"J\u2663";
	const wchar_t S12[] = L"Q\u2660"; const wchar_t C12[] = L"Q\u2663";
	const wchar_t S13[] = L"K\u2660"; const wchar_t C13[] = L"K\u2663";
	const int size = 52;
	const int nbplayers = 4;
	const int handSize = 5;
#pragma endregion

#pragma region fonctions Call Code du Main

	int wallet[5] = { 100,100,100,100,0 };
	int loot = 0;
	char playername[] = " ";
	int a = 8;
	int b = 6;
	char ans;

#pragma region TESTING ZONE

#pragma endregion

	char doRules = ' ';

	setCursorXY(65, 15);
	intro(a, b);																//================================================================================//
	setCursorXY(65, 25);													   //Ici se fait laffichage de l'accueil "Intro() ou on demande dentrer son nom et   //
	wcout << "Entrez votre nom: ";											  // s'il desire voir les reglements introrules(). Si le joueur entre Y pour oui le //
	cin >> playername;														 // programme passera la fonction introrules() avant de debuter la partie==========//
	setCursorXY(65, 26);
	wcout << "D�sirez-vous voir les r�gles du jeu ? Y|N : ";
	cin >> doRules;
	system("cls");

	if (doRules == 'y' || doRules == 'Y')
	{
		introrules(a, b);
		setCursorXY(65, 50);
		wcout << "Appuyez une touche pour continuer";
		system("pause>nul");
		system("cls");
	}
	do
	{
		int	HandStatusTab[4][2] = { 0 };
		wallet[4] = 0;
		for (int i = 0; i < 4; i++)
		{
			if (wallet[3] >= 10)
			{
				wallet[4] += 10;
				wallet[i] -= 10;
			}
			else
			{
				wcout << "Vous avez tout perdu, Fin de la partie." << endl;
				wcout << "Appuyez une touche pour quitter" << endl;
				system("pause>nul");
				exit(0);
			}
		}
#pragma region Tableaux!

		///Creation du tableau qui sert Paquet de cartes (partie visuel en char)
		const wchar_t* CardTab[size] = {
			H1,H2,H3,H4,H5,H6,H7,H8,H9,H10,H11,H12,H13,
			D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,
			S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,
			C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13
		};
		/// Creation du tableau analogue a mon paquet de carte qui vehicule la valeur de chaque cartes
		int ValTab[size] = {
			11,2,3,4,5,6,7,8,9,10,10,10,10,
			11,2,3,4,5,6,7,8,9,10,10,10,10,
			11,2,3,4,5,6,7,8,9,10,10,10,10,
			11,2,3,4,5,6,7,8,9,10,10,10,10,
		};
		///Creation du tableau des mains Visuels
		const wchar_t* CardHandTab[nbplayers][handSize] = { NULL };
		///Creation du tableau des mains Valeurs
		int ValHandTab[nbplayers][handSize] = { 0 };

#pragma endregion

		ans = ' ';

		setCursorXY(65, 25);																			//===============================================================================//
		wcout << "Appuyez une touche pour commencer";												   // La partie s'ouvre avec la fonction drawCards() qui distribue 2 cartes a chaque//
		system("pause > nul");																		  // joueurs. Le IF == false verifie si on a un gagnant puisque il est possible de //
		drawCards(CardTab, ValTab, CardHandTab, ValHandTab);                                         // remporter la partie lors du premier tour avec un NATURAL. Si cest le cas      //
																									// le programme passera a la fin grace a goto QuickEmd ==========================//
		showHands(ValHandTab, CardHandTab, wallet, HandStatusTab, playername);
		if (checkforWinner(ValHandTab, wallet, HandStatusTab, playername) == false)
		{
			draw_rectnoconditions(a, b, 0, 0, CardHandTab, ValHandTab);

			goto QuickEnd;
		}
		setCursorXY(65, 50);
		wcout << "Appuyez une touche pour continuer";
		system("pause>nul");
		setCursorXY(65, 50);
		wcout << "                                 ";

		for (int i = 0; i < 3; i++) // loop pour next round												      //============================================================================//
		{																									 //Cette loop gere le nombre de fois que la fonction anotherCard() sera appel�e//
			setCursorXY(65, 50);																			//Lorsque la loop atteint sont 3iem passage tout les joueurs sont mis out et  //
			anotherCard(CardTab, ValTab, CardHandTab, ValHandTab, wallet, HandStatusTab, playername);	   // la partie se termine. a la sortie de cette loop on dans le IF false, la    //
			if (i == 2)																					  // fonction draw_rectnocondition fera tourner la premiere carte de chaque et  //
			{																							 // et distribuera la mise parmi le/les gagnants. et passera ensuite a         //
				for (int j = 0; j < 4; j++)																// la fin du programme grace a goto QuickEnd.=================================//
				{
					//showHands(ValHandTab, CardHandTab, wallet, HandStatusTab, playername);//TODO // CHECK si absence cause bug
					HandStatusTab[i][2] = 1;
				}
				//anotherCard(CardTab, ValTab, CardHandTab, ValHandTab, wallet, HandStatusTab, playername); //TODO // CHECK si absence cause bug
			}
			if (checkforWinner(ValHandTab, wallet, HandStatusTab, playername) == false)
			{
				draw_rectnoconditions(a, b, 0, 0, CardHandTab, ValHandTab);

				goto QuickEnd;
			}
			setCursorXY(65, 50);
			wcout << "Appuyez une touche pour continuer";
			system("pause>nul");
			setCursorXY(65, 50);
			wcout << "                                 ";
		}

		goto EndGame;

	QuickEnd:
		draw_rectnoconditions(a, b, 0, 0, CardHandTab, ValHandTab);
		setCursorXY(65, 50);
		wcout << "Appuyez une touche pour continuer";
		system("pause>nul");

	EndGame:
		///Fin de la partie -- Nouvelle partie?
		system("cls");
		setCursorXY(65, 15);
		wcout << "---------Partie Termin�e!---------" << endl;
		setCursorXY(65, 15);
		intro(a, b);
		setCursorXY(65, 48);
		wcout << "Voulez vous faire une autre partie? y/n     " << endl;
		setCursorXY(65, 49);
		cin >> ans;

		system("cls");
	} while (ans == 'y' || ans == 'Y');
#pragma endregion

	///End Game
}